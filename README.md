Wunderman Thompson Assessment
========================

This document contains information on how to download, run and configure the assessment project.

Pre-installation
----------------

Make sure you have Symfony CLI, Git 1.8.5+, PHP 7.2+ and Docker installed on your machine.

Set up a local server for the assessment.


Installing
----------

Check out the source from BitBucket. By executing `git clone https://KoketsoMabuela@bitbucket.org/KoketsoMabuela/wt_assessment.git` on a
new terminal session in the location where you'd like to store the project files.

### Install dependencies

If you don't have Composer yet, download it following the instructions
[here](http://getcomposer.org/).

Then, install all the required dependencies:

    composer install

If you don't have Yarn yet, download it following the instructions
[here](https://yarnpkg.com/en/docs/getting-started).

Then, install all the required dependencies:

    yarn install

### Compile assets

After than, compile all assets by running:

    yarn dev


### Serve the assessment project

On the new terminal session start/serve the project by running:

    symfony serve -d

Then build the MySQL Docker image bundled in the project, and run it's container:

    docker-compose up -d

After the Docker CLI has provided CLI output the that MySQL is now rup and running,
you can verify that by running:

    docker-compose -ps


Getting started with Symfony
-------------------------------

A great way to start learning Symfony is via the [quick tour](https://symfony.com/doc/3.4/quick_tour/the_big_picture.html), which will
take you through all the basic features of Symfony 5.0. From there, you can learn more by reading the [Symfony documentation](https://symfony.com/doc/current/index.htm.).
