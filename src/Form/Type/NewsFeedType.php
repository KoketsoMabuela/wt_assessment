<?php
/*
 * This file is part of the Wunderman Thompson PHP Developer Assessment project.
 *
 * @author      Koketso Mabuela <glenton92@gmail.com>
 * @copyright   Copyright (c) Wunderman Thompson
 */

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\NewsFeed;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class NewsFeedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('deleted', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('type', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('author', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('time', DateType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('text', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('dead', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('parent', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('poll', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('kids', IntegerType::class)
            ->add('url', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('score', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('parts', IntegerType::class)
            ->add('descendants', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
           'data_class' => NewsFeed::class,
        ]);
    }
}