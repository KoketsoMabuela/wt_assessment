<?php
/*
 * This file is part of the Wunderman Thompson PHP Developer Assessment project.
 *
 * @author      Koketso Mabuela <glenton92@gmail.com>
 * @copyright   Copyright (c) Wunderman Thompson
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\NewsFeed;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

;

class NewsController extends AbstractAPIController
{
    public function retrieveAction(RequestStack $requestStack): Response
    {
        try {
            $newsType = $requestStack->getCurrentRequest()->attributes->get('type');
            $news = $this->getDoctrine()
                ->getRepository(NewsFeed::class)
                ->findBy([
                    'type' => $newsType,
                    'deleted' => false,
                    'dead' => false,
                ]);

            if (empty($news)) {
                return $this->json("Sorry! No $newsType Hacker news found at the moment.", 404);
            }

            return new Response(json_encode($news));
        } catch (\Exception $exception) {

            throw new ServiceUnavailableHttpException();
        }
    }

    public function createAction(Request $request): Response
    {
        try {
            $form = $this->buildForm(NewsFeed::class, '', []);
            $form->handleRequest($request);

            if (!$form->isSubmitted() || !$form->isValid()) {

                throw new BadRequestException();
            }

            /** @var NewsFeed $newsFeed */
            $newsFeed = $form->getData() ?? null;
            $this->getDoctrine()->getManager()->persist($newsFeed);
            $this->getDoctrine()->getManager()->flush();


            return $this->json('', 201);

        } catch (\Exception $exception) {

            throw new ServiceUnavailableHttpException();
        }
    }
}
