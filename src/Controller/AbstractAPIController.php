<?php
/*
 * This file is part of the Wunderman Thompson PHP Developer Assessment project.
 *
 * @author      Koketso Mabuela <glenton92@gmail.com>
 * @copyright   Copyright (c) Wunderman Thompson
 */

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;

abstract class AbstractAPIController extends AbstractController
{

    protected function buildForm(?string $type, ?string $data, ?array $options): FormInterface
    {
        return $this->container->get('form.factory')->createNamed('', $type, $data, $options);
    }
}