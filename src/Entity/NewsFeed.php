<?php
/*
 * This file is part of the Wunderman Thompson PHP Developer Assessment project.
 *
 * @author      Koketso Mabuela <glenton92@gmail.com>
 * @copyright   Copyright (c) Wunderman Thompson
 */

declare(strict_types=1);

namespace App\Entity;

use App\Repository\NewsFeedRepository;
use Cassandra\Date;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewsFeedRepository::class)
 */
class NewsFeed
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $deleted;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $type;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $time;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $text;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $dead;

    /**
     * @ORM\Column(type="integer")
     */
    private int $parentId;

    /**
     * @ORM\Column(type="integer")
     */
    private int $pollId;

    /**
     * @ORM\Column(type="text")
     */
    private array $commentIds;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $url;

    /**
     * @ORM\Column(type="integer")
     */
    private int $score;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private array $parts;

    /**
     * @ORM\Column(type="text")
     */
    private array $descendants;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getTime(): ?\DateTime
    {
        return $this->time;
    }

    public function setTime(\DateTime $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getDead(): ?bool
    {
        return $this->dead;
    }

    public function setDead(bool $dead): self
    {
        $this->dead = $dead;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function getPollId(): ?int
    {
        return $this->pollId;
    }

    public function setPollId(int $pollId): self
    {
        $this->pollId = $pollId;

        return $this;
    }

    public function getCommentIds(): ?array
    {
        return $this->commentIds;
    }

    public function setCommentIds(array $commentIds): self
    {
        $this->commentIds = $commentIds;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getParts(): ?array
    {
        return $this->parts;
    }

    public function setParts(array $parts): self
    {
        $this->parts = $parts;

        return $this;
    }

    public function getDescendants(): ?array
    {
        return $this->descendants;
    }

    public function setDescendants(array $descendants): self
    {
        $this->descendants = $descendants;

        return $this;
    }
}
