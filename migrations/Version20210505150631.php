<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505150631 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE news_feed (id INT AUTO_INCREMENT NOT NULL, deleted TINYINT(1) NOT NULL, type VARCHAR(20) NOT NULL, author VARCHAR(50) NOT NULL, time DATETIME NOT NULL, text VARCHAR(255) NOT NULL, dead TINYINT(1) NOT NULL, parent_id INT NOT NULL, poll_id INT NOT NULL, comment_ids INT NOT NULL, url VARCHAR(255) NOT NULL, score INT NOT NULL, title VARCHAR(255) NOT NULL, parts INT NOT NULL, descendants INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE news_feed');
    }
}
