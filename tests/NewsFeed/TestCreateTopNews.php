<?php
/*
 * This file is part of the Wunderman Thompson PHP Developer Assessment project.
 *
 * @author      Koketso Mabuela <glenton92@gmail.com>
 * @copyright   Copyright (c) Wunderman Thompson
 */

namespace App\Tests\NewsFeed;

use App\Entity\NewsFeed;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestCreateTopNews extends WebTestCase
{
    public function testCreateTopNewsSuccess(): void
    {
        $newFeed = new NewsFeed();
        $newFeed->setDeleted(false);
        $newFeed->setType('TOP');
        $newFeed->setAuthor('Koketso Test');
        $newFeed->setTime(new \DateTime());
        $newFeed->setText('Test TOP news');
        $newFeed->setDead(false);
        $newFeed->setParentId(0);
        $newFeed->setPollId(0);
        $newFeed->setCommentIds([1, 2]);
        $newFeed->setUrl('https://test.com/');
        $newFeed->setScore(100);
        $newFeed->setTitle('Test title');
        $newFeed->setParts([3, 4, 5, 6, 7]);
        $newFeed->setDescendants([8]);

        $payload = [
            'deleted' => $newFeed->getDeleted(),
            'type' => $newFeed->getType(),
            'author' => $newFeed->getAuthor(),
            'time' => $newFeed->getTime(),
            'text' => $newFeed->getText(),
            'dead' => $newFeed->getDead(),
            'parentId' => $newFeed->getParentId(),
            'pollId' => $newFeed->getPollId(),
            'commentIds' => $newFeed->getCommentIds(),
            'url' => $newFeed->getUrl(),
            'score' => $newFeed->getScore(),
            'title' => $newFeed->getTitle(),
            'descendants' => $newFeed->getDescendants(),
        ];

        $client = static::createClient();
        $client->request('POST', '/create/TOP', $payload);
        $this->assertResponseIsSuccessful();
    }

    public function testRetrieveTopNewsSuccess(): void
    {
        $client = static::createClient();
        $client->request('GET', '/news/TOP');
        $this->assertResponseIsSuccessful();
    }
}
